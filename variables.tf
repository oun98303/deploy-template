variable "ENVIRONMENT_NAME" {}

variable "SHORT_ENVIRONMENT_NAME" {}

variable "PG_ALLOCATED_STORAGE" {
  default = 20
  type    = number
}

variable "PG_ENGINE_VERSION" {
  default = "12.7"
  type    = string
}

variable "PG_INSTANCE_CLASS" {
  default = "db.t2.micro"
  type    = string
}

variable "EC2_INSTANCE_TYPE" {
  default = "t2.micro"
  type    = string
}

variable "SERVICE_DESK_EMAIL" {
  type = string
}

variable "SMTP_FROM" {
  default = ""
  type    = string
}

variable "REDIS_NODE_TYPE" {
  default = "cache.t2.micro"
  type    = string
}

variable "DISABLE_POSTGRES" {
  default = "false"
  type    = string
}

variable "DATABASE_DELETE_BACKUPS" {
  default = true
  type    = bool
}

variable "DISABLE_REDIS" {
  default = "false"
  type    = string
}


output "ENVIRONMENT_NAME" {
  value = var.ENVIRONMENT_NAME
}

output "SHORT_ENVIRONMENT_NAME" {
  value = var.SHORT_ENVIRONMENT_NAME
}

output "PG_ALLOCATED_STORAGE" {
  value = var.PG_ALLOCATED_STORAGE
}

output "PG_INSTANCE_CLASS" {
  value = var.PG_INSTANCE_CLASS
}

output "EC2_INSTANCE_TYPE" {
  value = var.EC2_INSTANCE_TYPE
}

output "SERVICE_DESK_EMAIL" {
  value = var.SERVICE_DESK_EMAIL
}

output "SMTP_FROM" {
  value = var.SMTP_FROM
}

output "REDIS_NODE_TYPE" {
  value = var.REDIS_NODE_TYPE
}

output "DISABLE_POSTGRES" {
  value = var.DISABLE_POSTGRES
}

output "DISABLE_REDIS" {
  value = var.DISABLE_REDIS
}

output "DATABASE_DELETE_BACKUPS" {
  value = var.DATABASE_DELETE_BACKUPS
}
